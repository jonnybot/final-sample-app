"use strict";

const express = require('express');
const hoffman = require('hoffman');
const path = require('path');
const bodyParser = require('body-parser');
const fileSystem = require('fs');

const app = express();
const listeningPort = parseInt(process.argv[2]) || 3000;
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'dust');
app.engine('dust', hoffman.__express());

let hominids = {
    "homo": [
        "sapiens"
    ],
    "pan": [
        "troglodytes"
    ]
};

let getHominidsByGenus = (genus) => {
    return hominids[genus].map((species) => {
        return {genus: genus, species: species};
    });
};

let getHominids = () => {
    return Object.keys(hominids).reduce((accumulator, genus) => {
        return accumulator.concat(getHominidsByGenus(genus));
    }, []);
};

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

let hominidRouter = express.Router();

hominidRouter.get("/", (request, response) => {
    response.render('index', {
        hominids: getHominids()
    });
});

hominidRouter.get("/:genus/", (request, response) => {
    let genus = request.params.genus;
    if (hominids[genus]) {
        response.render('genus', {
            genus: genus,
            species: hominids[genus]
        });
    } else {
        response.status(404).render('notfound', genus);
    }
});

hominidRouter.get("/:genus/:species", (request, response) => {
    let genus = request.params.genus;
    let species = request.params.species;
    let hominid = {
        genus: genus,
        species: species
    };
    if (hominids[genus] && hominids[genus].indexOf(species) !== -1) {
        response.render('hominid', hominid);
    } else {
        response.status(404).render('notfound', hominid);
    }
});

hominidRouter.post("/", (request, response) => {
    let genus = request.body.genus;
    let species = request.body.species;
    let knownGenus = hominids[genus];
    if (knownGenus) {
        knownGenus.push(species);
        hominids[genus] = knownGenus.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });
    } else {
        hominids[genus] = [species];
    }
    response.status(201).render('index', {
        hominids: getHominids()
    });
});

hominidRouter.get('/image/:genus/:species', (request, response) => {
    const {genus, species} = request.params;
    let imagePath = `${genus}_${species}.jpg`;
    if (fileSystem.existsSync(`assets/${imagePath}`)) {
        response.redirect(`/assets/${imagePath}`);
    } else {
        response.status(404);
        response.send(`No image found for ${genus} ${species}`);
    }
});

app.use('/hominid', hominidRouter);

app.get('/', (request, response) => {
    response.redirect('/hominid');
});

app.use('/assets', express.static('assets'));

app.listen(listeningPort, () => {
    console.log(`My app is listening on port ${listeningPort}!`);
});
