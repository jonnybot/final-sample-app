"use strict";

(function() {
    const showMonkeyPic = function(event) {
        let listItem = event.target;
        if (listItem.nodeName === 'A' || listItem.nodeName === 'IMG') {
            listItem = listItem.parentNode;
        }
        const picture = listItem.querySelector('img');
        if (picture) {
            console.log("Image already present for this picture");
        } else {
            let link = listItem.querySelector('a').getAttribute('href');
            link = link.replace(/hominid\//, 'hominid/image/');
            fetch(link).then( (response) => {
                if (response.ok) {
                    let image = document.createElement('img');
                    image.setAttribute('src', link);
                    listItem.appendChild(image);
                }
            });
        }
    };

    let loadHandler = function() {
        let hominids = document.querySelectorAll('.hominid.specific');
        hominids.forEach((listItem) => {
            listItem.addEventListener('mouseover', showMonkeyPic);
        });
    };

    window.addEventListener('load', loadHandler);
})();